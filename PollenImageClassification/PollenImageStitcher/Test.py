# -*- coding: utf-8 -*-
"""
Created on Tue Apr  7 13:01:42 2020

@author: CDS
"""

import cv2
import matplotlib.pyplot as plt

images = []

filepath = "input/"

print("Enter the number of images you want to concantenate:")
no_of_images = int(input())

for i in range(1,no_of_images+1):
    fname = filepath+"%d.jpg"%i
    img=cv2.imread(fname)
    images.append(img)


for i in range(no_of_images):
    plt.imshow(images[i])
    plt.show()
    
    