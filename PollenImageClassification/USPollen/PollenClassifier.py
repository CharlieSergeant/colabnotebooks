from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential, Model, load_model
from keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D
from keras import backend as k 
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TensorBoard, EarlyStopping
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.model_selection import StratifiedKFold
from tensorflow.keras.utils import to_categorical
import glob
import numpy as np
import os
import cv2
import matplotlib.pyplot as plt

# load and resize images 
#Output: data(size,size,3) -- the images labels(name) -- the classification type
def load_images(classes,size=224):
	data = []
	labels = []
	glober = []
	dim = (size, size) 
	with open(classes,"r") as f:
		for line in f.readlines():
			key = line.strip()
			glober = glob.glob("*"+key+"*.jpg")
			myClass = np.array(glober)
			for i in myClass:
				img = cv2.imread(i, cv2.IMREAD_UNCHANGED)
				resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
				data.append(resized)
				labels.append(key)
	f.close()
	return data, labels

#label the images
def label_images(data,labels):
	# convert the data into a NumPy array, then preprocess it by scaling
	# all pixel intensities to the range [0, 1]
	data = np.array(data, dtype="float") / 255.0
	# encode the labels (which are currently strings) as integers and then
	# one-hot encode them
	le = LabelEncoder()

	labels = le.fit_transform(labels)

	labels = to_categorical(labels, 42)

	return data, labels
 
#Creates VGG16 model with top 17 layers freezed and added custom layers for dropout
#and custom class size
def init_model():
	model = applications.VGG16(weights = "imagenet", include_top=False, input_shape = (224, 224, 3))

	for layer in model.layers[:17]:
			layer.trainable = False

	#Adding custom Layers 
	x = model.output
	x = Flatten()(x)
	x = Dense(256, activation="relu")(x)
	x = Dropout(0.3)(x)
	x = Dense(4096, activation="relu")(x)

	predictions = Dense(42, activation="softmax")(x)

	# creating the final model 
	#model_final = Model(input = model.input, output = predictions)
	model_final = Model(model.input,predictions)

	# compile the model 
	model_final.compile(loss = "categorical_crossentropy", optimizer = 'adam', metrics=["accuracy"])
	return model_final

#Output images,classification guess and the correct label
def output(testX,testY,labels):
	imgs = []

	y_pred = model.predict(testX) #take guess 
	y_pred = np.argmax(y_pred, axis=1)
	#Y_test = np.argmax(testY, axis=1)

	le = LabelEncoder()
	enc_lables = le.fit_transform(labels)
	classification_guess = le.inverse_transform(y_pred)

	for val,i in enumerate(testX):
		b,g,r = cv2.split(i)           # get b, g, r
		img = cv2.merge([r,g,b])
		imgs.append(img)
	
	return imgs,classification_guess,labels

def display_images(imgs,classifcation_guess,labels):
	for val,i in enumerate(imgs):
		plt.imshow(i)
		plt.show()
		print("Predicted: "+repr(classification_guess[val])+" Actual: "+repr(labels[val]))

	
